/*
  Oh! Multicast Video Scanner
  Copyright (C) 2016 Taeho Oh <ohhara@postech.edu>

  This file is part of Oh! Multicast Video Scanner.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _OMVS_GST_H_
#define _OMVS_GST_H_

#include <glib.h>

typedef void *OMVSGst;

extern OMVSGst omvs_gst_open(const gchar *uri, const gchar *filename);
extern gboolean omvs_gst_is_finished(OMVSGst gst);
extern gint omvs_gst_close(OMVSGst gst);

#endif /* _OMVS_GST_H_ */
