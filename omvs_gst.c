/*
  Oh! Multicast Video Scanner
  Copyright (C) 2016 Taeho Oh <ohhara@postech.edu>

  This file is part of Oh! Multicast Video Scanner.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <string.h>
#include <gst/gst.h>
#include "omvs_gst.h"

typedef struct _OMVSGstImpl {
  GstElement *play;
  GstBus *bus;
} OMVSGstImpl;

OMVSGst omvs_gst_open(const gchar *uri, const gchar *filename) {
  guint flags;
  OMVSGstImpl *gst_impl;
  gchar *protocol;
  gchar *location;
  gchar *real_uri;
  GstElement *play;
  GstElement *conv;
  GstElement *png;
  GstElement *file;
  GstElement *convpngfile;
  GstPad *pad;
  GstPad *ghostpad;

  if (gst_uri_is_valid(uri)) {
    protocol = gst_uri_get_protocol(uri);
    if (protocol) {
      location = gst_uri_get_location(uri);
      if (location) {
        if (!gst_uri_protocol_is_supported(GST_URI_SRC, protocol)) {
          if (strcmp(protocol, "rtp") == 0) {
            g_free(protocol);
            protocol = g_strdup("ortp");
          }
        }
        real_uri = g_strdup_printf("%s://%s", protocol, location);
        g_free(location);
      } else {
        real_uri = g_strdup(uri);
      }
      g_free(protocol);
    } else {
      real_uri = g_strdup(uri);
    }
  } else {
    real_uri = g_strdup(uri);
  }

  gst_impl = g_malloc0(sizeof(OMVSGstImpl));
  gst_impl->play = play = gst_element_factory_make("playbin", "play");
  g_object_set(G_OBJECT(play), "uri", real_uri, NULL);
  g_free(real_uri);
  g_object_get(play, "flags", &flags, NULL);
  flags &= (~0x00000002);
  g_object_set(play, "flags", flags, NULL);
  conv = gst_element_factory_make("queue", "conv");
  png = gst_element_factory_make("pngenc", "png");
  g_object_set(G_OBJECT(png), "snapshot", 1, "compression-level", 9, NULL);
  file = gst_element_factory_make("multifilesink", "file");
  g_object_set(G_OBJECT(file), "max-files", 1, "location", filename, NULL);

  convpngfile = gst_bin_new("convpngfile");
  gst_bin_add_many(GST_BIN(convpngfile), conv, png, file, NULL);
  gst_element_link_many(conv, png, file, NULL);
  pad = gst_element_get_static_pad(conv, "sink");
  ghostpad = gst_ghost_pad_new("sink", pad);
  gst_element_add_pad(convpngfile, ghostpad);
  gst_object_unref(GST_OBJECT(pad));

  g_object_set(play, "video-sink", convpngfile, NULL);
  g_object_set(play, "mute", TRUE, NULL);

  gst_impl->bus = gst_element_get_bus(play);
  gst_object_unref(GST_OBJECT(gst_impl->bus));

  gst_element_set_state(play, GST_STATE_PLAYING);

  return (OMVSGst)gst_impl;
}

gboolean omvs_gst_is_finished(OMVSGst gst) {
  OMVSGstImpl *gst_impl;
  GstMessage *msg;

  g_assert(gst);

  gst_impl = (OMVSGstImpl *)gst;

  msg = gst_bus_pop_filtered(gst_impl->bus, GST_MESSAGE_EOS);
  if (!msg) {
    return FALSE;
  }

  gst_message_unref(msg);

  return TRUE;
}

gint omvs_gst_close(OMVSGst gst) {
  OMVSGstImpl *gst_impl;

  gst_impl = (OMVSGstImpl *)gst;
  if (gst_impl) {
    gst_element_set_state(gst_impl->play, GST_STATE_NULL);
    gst_object_unref(gst_impl->play);
    g_free(gst_impl);
  }

  return 0;
}
